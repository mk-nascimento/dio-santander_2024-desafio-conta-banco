
import java.util.Locale;
import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) throws Exception {
        try (Scanner scan = new Scanner(System.in).useLocale(Locale.US)) {
            System.out.println("\nPor favor, Digite o número da Agência!");
            int agency = scan.nextInt();

            System.out.println("\nProssiga com o número da conta.");
            String account = scan.next();

            System.out.println("\nAgora, digite o seu nome:");
            String name = scan.next();

            System.out.println("\nsobrenome:");
            name = name.concat(" ").concat(scan.next());

            System.out.println("\nPor fim, quanto será depositado ?");
            double balance = scan.nextDouble();

            String greeting = "\nOlá ".concat(name);
            String thanks = ", obrigado por criar uma conta em nosso banco";
            String agencyInfo = ", sua agência é " + agency;
            String accInfo = ", conta ".concat(account);
            String currentBalanceInfo = " e seu saldo $" + balance;

            System.out.println(
                    greeting.concat(thanks)
                            .concat(agencyInfo)
                            .concat(accInfo)
                            .concat(currentBalanceInfo)
                            .concat(" já está disponível para saque!"));
        }
    }
}
